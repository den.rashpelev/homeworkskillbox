﻿#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Voice" << '\n';
	}
};
class Dog : public Animal
{
	void Voice() override
	{
		std::cout << "Bark!" << '\n';
	}
};
class Cat : public Animal
{
	void Voice() override
	{
		std::cout << "moew" << '\n';
	}
};
class Cow : public Animal
{
	void Voice() override
	{
		std::cout << "MOO" << '\n';
	}
};
int main()
{
	Animal* animals[3];
	animals[1] = new Dog();
	animals[0] = new Cat();
	animals[2] = new Cow();
	for (int i = 0; i < 3; i++)
	{
		animals[i]->Voice();
		delete animals[i];
	}
	return 0;
}